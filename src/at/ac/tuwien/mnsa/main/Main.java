package at.ac.tuwien.mnsa.main;

import java.security.Security;

import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CardTerminals;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
import javax.smartcardio.TerminalFactory;

import org.apache.log4j.PropertyConfigurator;

import at.ac.tuwien.mnsa.smartcard.MNSAProvider;

public class Main {

	static byte[] SELECT_CARD_MANAGER = {
		/* select */
		(byte) 0x00, (byte) 0xA4,
		/* using AID */
		(byte) 0x04, (byte) 0x00,
		/* with length = 8 */
		(byte) 0x08,
		/* AID of Card Manager = A000000003000000 */
		(byte) 0xA0, (byte) 0x00, (byte) 0x00, (byte) 0x00,
		(byte) 0x03, (byte) 0x00, (byte) 0x00, (byte) 0x00
	};

	/**
	 * Helper method to display a byte[] as String.
	 * 
	 * @param bytes
	 *        input byte[]
	 * @return a string representation of the input byte[]
	 */
	public static String bytesToHex(byte[] bytes) {
		final char[] hexArray = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
			'E', 'F' };
		char[] hexChars = new char[bytes.length * 2];
		int v;
		for (int j = 0; j < bytes.length; j++) {
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	public static void main(String[] arguments) throws Exception {
		PropertyConfigurator.configure("log4j.properties");
		 
		Security.addProvider(new MNSAProvider());
		TerminalFactory tf = TerminalFactory.getInstance("PC/SC", null, "MNSAProvider");
		
		CardTerminals cts = tf.terminals();
		for (CardTerminal ct : cts.list()) {
			System.out.println("Terminal '" + ct.getName() + "' fetched");
			if (ct.isCardPresent()) {
				System.out.println("Success (card present)");
				Card c = ct.connect("T=0");
				c.getATR();
				System.exit(0);
				CardChannel cc = c.getBasicChannel();
				ResponseAPDU res = cc.transmit(new CommandAPDU(SELECT_CARD_MANAGER));
				System.out.println("GET THIS: " + bytesToHex(res.getBytes()));
				c.disconnect(false);
			} else {
				System.out.println("Success (no card present)");
			}
		}
	}
}
