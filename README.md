nsa-test-dynamic-include
===========

This project tests the mobile NFCReader based, PC/SC card reader.
It includes the PCSC Reader jar dynamically as java provider during runtime. 
Therefore the JAR file "pcscReader.jar" from the project [nsa-desktop](https://github.com/ChristophPrybila/nsa-desktop) has to be included into the project's classpath and the Provider has to be registered by the program manually.

The TerminalFactory can then fetched via 

    TerminalFactory.getInstance("PC/SC", null, "MNSAProvider");
    
How to use
===========

- Our Provider uses Log4J therefore your program has to configure Log4J before fetching the TerminalFactory.

        PropertyConfigurator.configure("log4j.properties");
        
- Include "pcscReader.jar" from the project [nsa-desktop](https://github.com/ChristophPrybila/nsa-desktop) in your classpath.
- Next register the Provider with

        Security.addProvider(new MNSAProvider()); or
        Security.insertProviderAt(new MNSAProvider(), <POSITION>);

- Then fetch the TerminalFactory with

        TerminalFactory tf = TerminalFactory.getInstance("PC/SC", null, "MNSAProvider");
    
- Alternatively, if our Provider has the highest Priority of all TerminalFactories you can fetch it with 

        TerminalFactory.getDefault();
